title_map1 = {
    "PHTM":"PHTM",
    "CcHTMbest":"cc-HTM",
    "CcHTM":"cc-HTM",
    "PhysicalClocks":"NV-HTM",
    "LogicalClocks":"DudeTM",
    "PCWC-F":"Phys+Logic Clocks (w/ F)",
    "PCWC-NF":"Phys+Logic Clocks (w/o F)",
    "PSTM":"PSTM",
    "Crafty":"Crafty",
    "PCWM3":"SPHT, link back",
    "PCWM2":"SPHT, link next",
    "PCWM":"SPHT"
}
title_map1a = {
    "PHTM":"PHTM",
    "CcHTMbest":"cc-HTM",
    "CcHTM":"cc-HTM",
    "PhysicalClocks":"NV-HTM",
    "LogicalClocks":"Logic Clocks",
    "PCWC-F":"Phys+Logic Clocks (w/ F)",
    "PCWC-NF":"Phys+Logic Clocks (w/o F)",
    "PSTM":"PSTM",
    "PCWM3":"SPHT, link back",
    "PCWM2":"SPHT, link next",
    "PCWM":"SPHT"
}
title_map2 = {
    "PIN0":", NUMA",
    "PIN1":", SMT",
    "ASYNC":", async",
    "SYNC":", sync"
}
title_map3 = {
    "BUFFER-FLUSHES":", forward filter",
    "BACKWARD":", backward",
    "RANGE":", range",
    "ST-CLWB":", store/CLWB",
    "BUFFER-WBINVD":", wbinvd"
}
title_map4 = {
    "REP2":", 2r",
    "REP3":", 3r",
    "REP4":", 4r",
    "REP8":", 8r"
}
title_map5 = {
    "HEAP2097152B":", 2MB",
    "HEAP33554432B":", 32MB",
    "HEAP536870912B":", 512MB",
    "L134217720":", 10x",
    "L67108860":", 5x",
    "L6710886":", 0.5x"
}

def map_title_a(curTitle):
  tmp = title_map1
  title_map1 = title_map1a
  res = map_title(curTitle)
  title_map1 = tmp
  return res

def map_title(curTitle):
  res = ""
  for a in title_map1:
    if a in curTitle:
      res = title_map1[a]
      break
  if len(res) == 0:
    return curTitle
  for a in title_map2:
    if a in curTitle:
      res += title_map2[a]
      break
  if "WOPT" in curTitle:
    res += ", opt"
  for a in title_map3:
    if a in curTitle:
      res += title_map3[a]
      break
  for a in title_map4:
    if a in curTitle:
      res += title_map4[a]
      break
  for a in title_map5:
    if a in curTitle:
      res += title_map5[a]
      break
  return res
