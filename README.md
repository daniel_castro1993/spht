# FAST'21 paper28 SPHT

The focus is to stress the capabilities of a given strategy to commit a PTM transaction (HTM+PM).

## Prerequisites

An Intel machine with TSX enabled. Follow the info in the rsync.sh script.

There is a ```deps/``` folder with all the dependencies, compile each first.

## What the code do

Currently there are 5 different implementations for the commit phase:  
  -  [Logical Clock](nvhtm/src/impl_lc.c) - uses a logical clock to serialize transactions in the log (our prototype of DudeTM)  
  -  [Physical Clock](nvhtm/src/impl_pc.c) - logged transactions are sorted via monotonic, non-contiguos counter (our prototype of NV-HTM)  
  -  [ccHTM](nvhtm/src/impl_ccHTM.c) - The background process flushes the most recent queued transaction upon commit (our prototype of ccHTM)  
  -  [Crafty](nvhtm/src/impl_crafty.c) - our prototype of Crafty  
  -  [SPHT-NL](nvhtm/src/impl_pcwm.c) - our prototype of SPHT without links  
  -  [SPHT-FL](nvhtm/src/impl_pcwm2.c) - our prototype of SPHT with forward links  
  -  [SPHT-BL](nvhtm/src/impl_pcwm3.c) - our prototype of SPHT with backward links  

### Solutions that we are not using:

  -  [Epoch "Patient"](nvhtm/src/impl_epoch_sync.c) - transactions are logged in "batches" of N threads, if 1 thread is slow, then the other N-1 will have to wait  
  -  [Epoch "Impatient"](nvhtm/src/impl_epoch_impa.c) - solves the previous problem by imposing more synchronization (allows to "steal" a log slot from another thread if it is too slow)  
  -  [Physical+Logical](nvhtm/src/impl_pcwc.c) - WIP, the idea is to devise the logical clock from existing information, without having it actually mantaining it  
